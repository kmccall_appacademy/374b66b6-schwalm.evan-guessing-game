# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  answer = rand(1..100)
  num_guesses = 0

  loop do
    puts 'Guess a number!'
    guess = Integer(gets.chomp)
    num_guesses += 1

    case guess <=> answer
    when -1
      puts "#{guess} is too low!"
    when 0
      puts "You found the number! #{guess}"
      break
    when 1
      puts "#{guess} is too high!"
    end
  end

  puts "it took you #{num_guesses} guesses"
end

def file_shuffler(filename)
  name = File.basename(filename, ".*")
  ext = File.extname(filename)
  File.open("#{name}-shuffled#{ext}", "w") do |f|
    File.readlines(filename).shuffle.each do |l|
      f.puts l.chomp
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 1
    file_shuffler(ARGV.shift)
  else
    puts "Enter filename to shuffle:"
    filename = gets.chomp
    file_shuffler(filename)
  end
end
